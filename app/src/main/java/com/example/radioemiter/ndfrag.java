package com.example.radioemiter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ndfrag extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_nd, container, false);
        String[] items = {"12:00 - 15:00 - Filmowe Hity", "15:00 - 18:00 - Muzyczne Święta"};

        ListView listView = (ListView) view.findViewById(R.id.lv);

        ArrayAdapter<String> listviewadapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                items

        );

        listView.setAdapter(listviewadapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), Powiadomienie.class);
                startActivity(intent);
            }


        });


        return view;
    }
}
