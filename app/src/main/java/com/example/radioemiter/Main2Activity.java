package com.example.radioemiter;

import android.os.Build;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;

import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;


public class Main2Activity extends AppCompatActivity {

    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    PageAdapter pageAdapter;
    TabItem tabpon;
    TabItem tabwt;
    TabItem tabsr;
    TabItem tabcw;
    TabItem tabpt;
    TabItem tabsob;
    TabItem tabnd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ram_activity);

        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Ramówka");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tabLayout = findViewById(R.id.tablayout);
        tabpon = findViewById(R.id.tabpon);
        tabwt = findViewById(R.id.tabwt);
        tabsr = findViewById(R.id.tabsr);
        tabcw = findViewById(R.id.tabcw);
        tabpt = findViewById(R.id.tabpt);
        tabsob = findViewById(R.id.tabsob);
        tabnd = findViewById(R.id.tabnd);
        viewPager = findViewById(R.id.viewPager);

        pageAdapter = new PageAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pageAdapter);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 1) {
                    toolbar.setBackgroundColor(ContextCompat.getColor(Main2Activity.this,
                            R.color.black));
                    tabLayout.setBackgroundColor(ContextCompat.getColor(Main2Activity.this,
                            R.color.black));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        getWindow().setStatusBarColor(ContextCompat.getColor(Main2Activity.this,
                                R.color.black));
                    }
                } else if (tab.getPosition() == 2) {
                    toolbar.setBackgroundColor(ContextCompat.getColor(Main2Activity.this,
                            android.R.color.black));
                    tabLayout.setBackgroundColor(ContextCompat.getColor(Main2Activity.this,
                            android.R.color.black));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        getWindow().setStatusBarColor(ContextCompat.getColor(Main2Activity.this,
                                android.R.color.black));
                    }
                } else {
                    toolbar.setBackgroundColor(ContextCompat.getColor(Main2Activity.this,
                            R.color.black));
                    tabLayout.setBackgroundColor(ContextCompat.getColor(Main2Activity.this,
                            R.color.black));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        getWindow().setStatusBarColor(ContextCompat.getColor(Main2Activity.this,
                                R.color.black));
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

    }
}
