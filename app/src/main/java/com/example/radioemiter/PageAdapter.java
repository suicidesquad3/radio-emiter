package com.example.radioemiter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class PageAdapter extends FragmentPagerAdapter {

    private int numOfTabs;

    PageAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ponfrag();
            case 1:
                return new wtfrag();
            case 2:
                return new srfrag();
            case 3:
                return new cwfrag();
            case 4:
                return new ptfrag();
            case 5:
                return new sobfrag();
            case 6:
                return new ndfrag();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
