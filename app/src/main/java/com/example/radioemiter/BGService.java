package com.example.radioemiter;

import android.app.Service;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;

//private MediaPlayer Mediaplayer;

public class BGService extends Service {
    MediaPlayer mediaPlayer;

    boolean prepared = false;
    boolean started = false;
    //boolean prepared = false;
    String stream = "http://pldm.ml/radio?url=https://www.eskago.pl/radio/eska-rock";
    //@Nullable

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate(){


        Toast.makeText(this,"service created", Toast.LENGTH_SHORT).show();
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

       new PlayerTask().execute(stream);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        Toast.makeText(this,"service created", Toast.LENGTH_SHORT).show();
        mediaPlayer.start();

        return super.onStartCommand(intent, flags,startId);
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this,"service destroyed", Toast.LENGTH_SHORT).show();
            mediaPlayer.stop();
        super.onDestroy();
        if(prepared){
            mediaPlayer.release();
        }
    }
    class PlayerTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... strings) {
            try {
                mediaPlayer.setDataSource(strings[0]);
                //mediaPlayer.prepare();
                prepared = true;
            } catch (IOException e) {
                e.printStackTrace();
          }

            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

         //   b_play.setEnabled(true);
         //   b_play.setText("GRAJ");


        }

    }

//    @Override
//    protected void onPause() {
//        super.onPause();
//        if (started) {
//            mediaPlayer.pause();
//        }
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (started) {
//            mediaPlayer.start();
//        }
//    }




  //  @Override
   // protected void onDestroy() {
//        super.onDestroy();
//        if(prepared){
//            mediaPlayer.release();
//        }
  //  }
}
//}
   // @Override
//    //public int onStartCommand(Intent intent, int flags, int startId) {
//        return super.onStartCommand(intent, flags, startId);
//
//        MediaPlayer  Mediaplayer
//        Mediaplayer.start();
//        return START_STICKY;
//    }
//
//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        player.stop();
//    }
//}
