package com.example.radioemiter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class ponfrag extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frag_pon, container, false);
        String[] items = {"09:00 - 11:00 - Poranek z Radiem Emiter - Zespół Radia Emiter", "15:00 - 18:00 - Blok Rap", "21:00 - 23:00 - M.M.Mam Pytanie - Miśka Wyszyńska, Misiek Majewski"};

        ListView listView = (ListView) view.findViewById(R.id.lv);

        ArrayAdapter<String> listviewadapter = new ArrayAdapter<String>(
                getActivity(),
                android.R.layout.simple_list_item_1,
                items

        );

        listView.setAdapter(listviewadapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(), Powiadomienie.class);
                startActivity(intent);
            }


        });





        return view;
    }
}
