package com.example.radioemiter;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.MovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.w3c.dom.Text;

import java.io.IOException;
import java.util.Calendar;
import java.util.Random;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

public class MainActivity extends AppCompatActivity {
    TextView mTextView;
    Button przejdz;
    Button b_play;
    Button notification;
    Button mail;
    MediaPlayer mediaPlayer;
    boolean prepared = false;
    boolean started = false;
    String stream = "http://pldm.ml/radio?url=https://www.eskago.pl/radio/eska-rock";
    Button nagrywanie;
    Button akt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTextView = findViewById(R.id.tekst);
        new doit().execute();

        b_play = (Button) findViewById(R.id.b_play);
        b_play.setEnabled(false);
        b_play.setText("LADOWANIE");
        mediaPlayer = new MediaPlayer();
        mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        new PlayerTask().execute(stream);


       przejdz = (Button) findViewById(R.id.przejdz);
       przejdz.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               openactivity2();
           }
       });

        nagrywanie = (Button) findViewById(R.id.nagrywanie);
        nagrywanie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                opennagrywanie();
            }
        });
        //nowe
        notification = (Button) findViewById(R.id.powiadomienie1);
        notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPowiadomienie();
            }
        });

        akt = (Button) findViewById(R.id.akt);
        akt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openaktualnosci();
            }
        });

        mail = (Button) findViewById(R.id.mail);
        mail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openMail();
            }
        });


        b_play.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if (started) {
                    started = false;
                    mediaPlayer.stop();
                    mediaPlayer.prepareAsync();
                    Intent myService = new Intent(MainActivity.this, BackgroundSoundService.class);
                    stopService(myService);
                    b_play.setText("start");
                } else {
                    started = true;
                    //mediaPlayer.start();
                    Intent myService = new Intent(MainActivity.this, BackgroundSoundService.class);
                    startService(myService);
                    b_play.setText("Stop");
                }

            }


        });
    }





    public void openactivity2(){

        Intent intent = new Intent(this, Main2Activity.class);
        startActivity(intent);

    }

    public void openaktualnosci(){

        Intent intent = new Intent(this, Aktualnosci.class);
        startActivity(intent);

    }

    public void opennagrywanie(){
        Intent intent = new Intent(this, nagrywanie.class);
        startActivity(intent);
    }

    //tutaj zmienic na powiadomienie
    public void openPowiadomienie(){
        Intent intent = new Intent(this, Powiadomienie.class);
        startActivity(intent);
    }

    public void openMail(){
        Intent intent = new Intent(this, Mail.class);
        startActivity(intent);
    }


    public class doit extends AsyncTask<Void, Void, Void> {
        String song;

        @Override
        protected Void doInBackground(Void... voids) {
            try {

                    Document document = Jsoup.connect("http://online.radioemiter.pl/").get();
                    song = document.select("#gramy2").text();


            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mTextView.setText(song);
        }
    }

    class PlayerTask extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... strings) {
            try {
                mediaPlayer.setDataSource(strings[0]);
                mediaPlayer.prepare();
                prepared = true;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return prepared;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            b_play.setEnabled(true);
            b_play.setText("GRAJ");
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (started) {
            mediaPlayer.pause();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (started) {
            mediaPlayer.start();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (prepared) {
            mediaPlayer.release();
        }
    }





}